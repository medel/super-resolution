import time
import torch.backends.cudnn as cudnn
import torchvision
from torch import nn
from models import Generator, Discriminator 
from datasets import SRDataset
from utils import *

class TruncatedVGG19(nn.Module):
    def __init__(self, i=5, j=4):
        super(TruncatedVGG19, self).__init__()

        vgg19 = torchvision.models.vgg19(pretrained=True)
        maxpool_counter = 0
        conv_counter = 0
        truncate_at = 0
        for layer in vgg19.features.children():
            truncate_at += 1

            if isinstance(layer, nn.Conv2d):
                conv_counter += 1
            if isinstance(layer, nn.MaxPool2d):
                maxpool_counter += 1
                conv_counter = 0
            if (maxpool_counter == i - 1) and (conv_counter == j):
                break

        self.truncated_vgg19 = nn.Sequential(*list(vgg19.features.children())[:truncate_at + 1])

    def forward(self, input):
        output = self.truncated_vgg19(input)
        return output

def train(train_loader,
          generator,
          discriminator,
          truncated_vgg19,
          content_loss_criterion,
          adversarial_loss_criterion,
          optimizer_generator,
          optimizer_discriminator,
          epoch):
   
    losses_content = AverageMeter()
    losses_generator = AverageMeter()
    losses_discriminator = AverageMeter()
 
    generator.train()
    discriminator.train()
    
    start = time.time()

    for i, (lr_imgs, hr_imgs) in enumerate(train_loader):
        data_time.update(time.time() - start)

        lr_imgs = lr_imgs.to(device)
        hr_imgs = hr_imgs.to(device)

        sr_imgs = generator(lr_imgs)
        sr_imgs = convert_image(sr_imgs, source='[-1, 1]', target='imagenet-norm')

        sr_imgs_in_vgg_space = truncated_vgg19(sr_imgs)
        hr_imgs_in_vgg_space = truncated_vgg19(hr_imgs).detach()

        sr_discriminated = discriminator(sr_imgs)  # (N)

        content_loss = content_loss_criterion(sr_imgs_in_vgg_space, hr_imgs_in_vgg_space)
        adversarial_loss = adversarial_loss_criterion(sr_discriminated, torch.ones_like(sr_discriminated))
        perceptual_loss = content_loss + 1e-3 * adversarial_loss

        optimizer_generator.zero_grad()
        perceptual_loss.backward()

        optimizer_generator.step()

        losses_content.update(content_loss.item(), lr_imgs.size(0))
        losses_generator.update(adversarial_loss.item(), lr_imgs.size(0))

        hr_discriminated = discriminator(hr_imgs)
        sr_discriminated = discriminator(sr_imgs.detach())

        adversarial_loss = adversarial_loss_criterion(
            sr_discriminated, torch.zeros_like(sr_discriminated)) + adversarial_loss_criterion(
            hr_discriminated, torch.ones_like(hr_discriminated))

        optimizer_discriminator.zero_grad()
        adversarial_loss.backward()

        optimizer_discriminator.step()
        losses_discriminator.update(adversarial_loss.item(), hr_imgs.size(0))

        if i % 100 == 0:
            print('Epoch: [{0}][{1}/{2}]----'
                  'Cont. Loss {loss_c.val:.4f} ({loss_c.avg:.4f})----'
                  'Adv. Loss {loss_a.val:.4f} ({loss_a.avg:.4f})----'
                  'Disc. Loss {loss_d.val:.4f} ({loss_d.avg:.4f})'.format(
                      epoch,
                      i,
                      len(train_loader),
                      loss_c=losses_content,
                      loss_a=losses_generator,
                      loss_d=losses_discriminator))

    del lr_imgs
    del hr_imgs
    del sr_imgs
    del hr_imgs_in_vgg_space
    del sr_imgs_in_vgg_space
    del hr_discriminated
    del sr_discriminated

if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    cudnn.benchmark = True

    data_folder = './'
    scaling_factor = 4

    # Initialize the Generator..
    generator = Generator()

    # Initialize generator using SRResNet.
    generator.initialize('checkpoints/srresnet.pth.tar')

    # Initialize generator optimizer
    optimizer_generator = torch.optim.Adam(
        params=filter(lambda p: p.requires_grad,
        generator.parameters()),
        lr=1e-4)

    # Initialize the Discriminator.
    discriminator = Discriminator()

    # Initialize discriminator optimizer.
    optimizer_discriminator = torch.optim.Adam(
        params=filter(lambda p: p.requires_grad,
        discriminator.parameters()),
        lr=1e-4)

    truncated_vgg19 = TruncatedVGG19()
    truncated_vgg19.eval()

    # Loss functions.
    content_loss_criterion = nn.MSELoss()
    adversarial_loss_criterion = nn.BCEWithLogitsLoss()

    generator = generator.to(device)
    discriminator = discriminator.to(device)
    truncated_vgg19 = truncated_vgg19.to(device)
    content_loss_criterion = content_loss_criterion.to(device)
    adversarial_loss_criterion = adversarial_loss_criterion.to(device)

    train_dataset = SRDataset(data_folder,
                              split='train',
                              crop_size=96,
                              scaling_factor=scaling_factor,
                              lr_img_type='imagenet-norm',
                              hr_img_type='imagenet-norm')

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=16, shuffle=True, pin_memory=True)

    iterations = 200000
    for epoch in range(0, int(iterations // len(train_loader) + 1)):
      if epoch == int((iterations / 2) // len(train_loader) + 1):
        adjust_learning_rate(optimizer_generator, 0.1)
        adjust_learning_rate(optimizer_discriminator, 0.1)

      train(train_loader=train_loader,
            generator=generator,
            discriminator=discriminator,
            truncated_vgg19=truncated_vgg19,
            content_loss_criterion=content_loss_criterion,
            adversarial_loss_criterion=adversarial_loss_criterion,
            optimizer_g=optimizer_generator,
            optimizer_d=optimizer_discriminator,
            epoch=epoch)

      torch.save({'epoch': epoch,
                  'generator': generator,
                  'discriminator': discriminator,
                  'optimizer_generator': optimizer_generator,
                  'optimizer_discriminator': optimizer_discriminator},
                  'sr.pth.tar')
