#!/usr/bin/env python

from tflite_runtime.interpreter import Interpreter

import cv2
from PIL import Image
import numpy as np
import sys, os

def set_input_tensor(interpreter, image):
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image

if len(sys.argv) != 3:
  print("usage: predict.py [image] [model]")
  sys.exit(2)

interpreter = Interpreter(sys.argv[2])
interpreter.allocate_tensors()
_, height, width, _ = interpreter.get_input_details()[0]['shape']

image = Image.open(sys.argv[1]).convert('RGB').resize((width, height), Image.ANTIALIAS)
image= np.asarray(image) / 255

set_input_tensor(interpreter, image)
interpreter.invoke()
output_details = interpreter.get_output_details()

output = interpreter.get_tensor(0)[0,:,:,:]

sr_img = output.clip(0, 1) * 255
sr_img = np.uint8(sr_img)

filename = os.path.basename(sys.argv[1])
base = os.path.splitext(filename)[0]
Image.fromarray(sr_img).save('out-' + base + '.png')

