#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/string_util.h"
#include "tensorflow/lite/model.h"

#include <iostream>
#include <opencv2/opencv.hpp>

int main(int argc, char** argv)
{
  if (argc != 3)
  {
    std::cout << "usage: predict.py [image] [model]\n";
    exit(2);
  }

  // Load model
  std::unique_ptr<tflite::FlatBufferModel> model =
  tflite::FlatBufferModel::BuildFromFile(argv[2]);
		
  // Build the interpreter
  tflite::ops::builtin::BuiltinOpResolver resolver;
  std::unique_ptr<tflite::Interpreter> interpreter;
  tflite::InterpreterBuilder(*model.get(), resolver)(&interpreter);

  // Resize input tensors, if desired.
  cv::Mat image0 = imread(argv[1], cv::IMREAD_COLOR);
  cv::Mat image;
  resize(image0, image, cv::Size(480,270));
  cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
  image /= 255.0;

  interpreter->AllocateTensors();
  interpreter->SetNumThreads(8);
  memcpy(interpreter->typed_input_tensor<float>(0), image.data, image.total() * image.elemSize());
  
  interpreter->Invoke();

  TfLiteTensor* output = interpreter->tensor(interpreter->outputs()[0]);
  
  return 0;
}
