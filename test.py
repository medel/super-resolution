import torch
from PIL import Image
import torchvision.transforms.functional as FT

def convert_image(img, source, target):
    imagenet_mean = torch.FloatTensor(
        [0.485, 0.456, 0.406]).unsqueeze(1).unsqueeze(2)
    imagenet_mean_cuda = torch.FloatTensor(
        [0.485, 0.456, 0.406]).to(device).unsqueeze(0).unsqueeze(2).unsqueeze(3)
    imagenet_std = torch.FloatTensor(
        [0.229, 0.224, 0.225]).unsqueeze(1).unsqueeze(2)
    imagenet_std_cuda = torch.FloatTensor(
        [0.229, 0.224, 0.225]).to(device).unsqueeze(0).unsqueeze(2).unsqueeze(3)

    if source == 1:
      img = FT.to_tensor(img)
    elif source == 2:
      img = (img + 1.) / 2.

    if target == 1:
      img = FT.to_pil_image(img)
    elif target == 2:
      if img.ndimension() == 3:
        img = (img - imagenet_mean) / imagenet_std
      elif img.ndimension() == 4:
        img = (img - imagenet_mean_cuda) / imagenet_std_cuda

    return img

if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    srgan_generator = torch.load('checkpoints/sr.pth.tar')['generator'].to(device)
    srgan_generator.eval()

    # Downscale to obtain low-res version of the image.
    hr_image = Image.open('test.png', mode="r").convert('RGB')
    lr_image = hr_image.resize(
        (int(hr_image.width / 4), int(hr_image.height / 4)),
        Image.BICUBIC)

    # Bicubic image upscaling. 
    bicubic_image = lr_image.resize(
        (hr_image.width, hr_image.height),
        Image.BICUBIC)
    bicubic_image.save('bicubic-out.png')

    # Super-resolution upscaling. 
    sr_image = srgan_generator(
        convert_image(lr_image, 1, 2).unsqueeze(0).to(device))
    sr_image = sr_image.squeeze(0).cpu().detach()
    sr_image = convert_image(sr_image, 2, 1)
    sr_image.save('sr-out.png')
